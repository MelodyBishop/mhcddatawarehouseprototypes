
USE [DataMarts]
GO

DROP PROCEDURE [Reports].[JCMH_SP_Run_Allergies]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--execute  [Reports].[JCMH_SP_Run_Allergies] 
--select * from [Reports].[Allergies] order by 1
/*------------------------------------------------------
Create a Warehouse version of Allergies reporting for dashboard- Melody Bishop 6/22/2018
------------------------------------------------------*/
Create Procedure [Reports].[JCMH_SP_Run_Allergies] 
As
Begin
Truncate table [Reports].[Allergies]
Insert into [Reports].[Allergies]

SELECT [ID]  [Allergy Row ID]
      ,pcd.patient_Name + '-' + Cast(a.PATID as varchar(10)) Consumer
	  ,a.patid [Consumer ID]
	  ,DateDiff(yy,pcd.Date_of_Birth,Onset_Date) [Age At Onset]
      ,[allergy_name] [Allergen]
      ,[allergy_type] [Allergy Type]
      ,[date_noted]   [Date Recognized] --verify this is what they use...not in Data Disctionary
      ,[onset_date]   [Date Recorded]
      ,[reaction] Reaction
      ,[severity] Severity
  FROM [AvatarDW].[CWSInfoScrb].[Allergies] a
  left join AvatarDW.System.Patient_Current_Demographics pcd
  on pcd.patid = a.patid
  and pcd.facility= a.facility
  End
  GO


  