
USE [DataMarts]
GO

DROP TABLE Reports.Allergies
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE Reports.Allergies(
		[Allergy Row Id] varchar(254) NULL,
		[Consumer] Varchar(200) NULL,
	    [Consumer ID] Varchar(10) NULL,
        [Age At Onset] int NULL,
        [Allergen] varchar(200) NULL,
        [Allergy Type] varchar(25) NULL,
        [Date Recognized] date NULL, --verify this is what they use...not in Data Dictionary
        [Date Recorded] date NULL,
        [Reaction] varchar(20) NULL,
        [Severity] varchar(20) NULL
) ON [PRIMARY]
GO


